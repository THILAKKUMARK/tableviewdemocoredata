//
//  AppDelegate.swift
//  DemoTablview
//
//  Created by Dinesh Sunder on 3/3/19.
//  Copyright © 2019 Dinesh Sunder. All rights reserved.
//
import Foundation
import CoreData


extension Profiles {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Profiles> {
        return NSFetchRequest<Profiles>(entityName: "Profiles")
    }
    
    @NSManaged public var email: String?
    @NSManaged public var imageUrl: String?
    @NSManaged public var name: String?
    
    @NSManaged public var indiData : ALLProfile?
}
