//
//  AppDelegate.swift
//  DemoTablview
//
//  Created by Dinesh Sunder on 3/3/19.
//  Copyright © 2019 Dinesh Sunder. All rights reserved.
//

import Foundation
import UIKit

extension WebService{
    
    func dataFromAPI(emailId : String?,block : @escaping WSCompletionBlock){
        let service = WebServiceToGetAllDataFromAPI(manager: self, emailId: emailId, block: block)
        self.startRequest(service: service)
        
    }
}
