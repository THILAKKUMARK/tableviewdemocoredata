//
//  AppDelegate.swift
//  DemoTablview
//
//  Created by Dinesh Sunder on 3/3/19.
//  Copyright © 2019 Dinesh Sunder. All rights reserved.
//

import Foundation
import Alamofire

class WebServiceToGetAllDataFromAPI : WebServiceRequest{
    
    init(manager : WebService,emailId:String?, block : @escaping WSCompletionBlock) {
        
        super.init(manager : manager, block: block)
        
        url = manager.URL + "/list"
        
        if let emailId=emailId{
            self.body?["emailId"]=emailId
        }
        
        httpMethod = HTTPMethod.post
    }
    
    override func responseSuccess(data : Any?){
        super.responseSuccess(data: data)
    }
    
    
}

